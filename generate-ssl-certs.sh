#!/usr/bin/env bash
echo Setting up local SSL certs...
# https://www.py4u.net/discuss/13430

mkdir -p ssl
openssl req -newkey 2048 -keyout ssl/cert.key -nodes -x509 -out ssl/cert.crt -batch
