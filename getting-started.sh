#!/usr/bin/env bash
echo Running getting-started.sh ...

cd ./src

if [ -e .env ]
then
    echo Environment file already exists, skipping copy...
else
  cp .env.example .env  
fi

python3 -m venv venv

# SSL
cd ..
sh generate-ssl-certs.sh

# sqlite
echo Creating database.sqlite3 file from schema...
python3 src/init_db.py

echo "Done."
echo "Run 'source src/venv/bin/activate' to activate the python virtual environment."
