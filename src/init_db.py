import sqlite3
import os
from dotenv import load_dotenv
load_dotenv()

database_file_path = os.getenv('SQLITE3_DATABASE_FILE')
database_exists = os.path.exists(database_file_path)

# If the database exists
if database_exists:
    # Exit early without dropping tables
    exit()

# Else, create the database
connection = sqlite3.connect(database_file_path)

current_directory = os.path.dirname(os.path.abspath(__file__))
schema_file_path = os.path.join(current_directory, 'schema.sql')

with open(schema_file_path) as f:
    # execute schema
    connection.executescript(f.read())

    # get cursor
    cursor = connection.cursor()

    # database name (default is "main")
    cursor.execute("""
       PRAGMA database_list;
    """)
    print(cursor.fetchall())

    # show tables
    cursor.execute("""
        SELECT * 
        FROM main.sqlite_master 
        WHERE type='table';
    """)
    print(cursor.fetchall())

    # disconnect
    connection.close()
