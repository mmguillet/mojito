PRAGMA foreign_keys=OFF;
/*
 * https://plaid.com/exchange/docs/data-dictionary/
 */

/* schema */
BEGIN TRANSACTION;

DROP TABLE IF EXISTS plaidAccount;
CREATE TABLE plaidAccount (
    id INTEGER PRIMARY KEY AUTOINCREMENT, -- numeric id, does not come from plaid
    accountId TEXT NOT NULL UNIQUE,
    lastActivityAt TEXT DEFAULT NULL,
    ownershipType TEXT,
    ownerIdentityIds TEXT DEFAULT "[]", -- json string array
    nonOwnerIdentityIds TEXT DEFAULT "[]", -- json string array
    status TEXT,
    accountType TEXT,
    accountSubtype TEXT,
    accountName TEXT,
    officialName TEXT,
    displayMask TEXT,
    openingDate TEXT,
    currentBalance TEXT, -- currency, use and store as decimal(19,4)
    availableBalance TEXT, -- currency, use and store as decimal(19,4)
    taxAdvantaged INTEGER, -- boolean
    currency TEXT,
    nonIsoCurrency TEXT,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX plaidAccount_accountId ON plaidAccount(accountId);

DROP TRIGGER IF EXISTS plaidAccount_afterUpdate;
CREATE TRIGGER plaidAccount_afterUpdate
    AFTER UPDATE
    ON plaidAccount
    WHEN OLD.updatedAt IS NULL OR NEW.updatedAt < OLD.updatedAt -- If the recursive_triggers config variable is true, then this "NEW < OLD" check prevents an infinite loop
        BEGIN
            UPDATE plaidAccount
            SET updatedAt = CURRENT_TIMESTAMP
            WHERE id = OLD.id;
        END;

DROP TABLE IF EXISTS plaidTransaction;
CREATE TABLE plaidTransaction (
    id INTEGER PRIMARY KEY AUTOINCREMENT, -- numeric id, does not come from plaid
    transactionId TEXT NOT NULL UNIQUE,
    accountId TEXT NOT NULL, -- foreign key to plaidAccount table
    amount TEXT NOT NULL, -- currency, use and store as decimal(19,4)
    isoCurrencyCode TEXT NOT NULL,
    unofficialCurrencyCode TEXT DEFAULT NULL,
    category TEXT DEFAULT "[]", -- json string array
    categoryId INTEGER,
    checkNumber TEXT DEFAULT NULL, -- might be integer?
    transactionDate TEXT,
    transactionDatetime TEXT,
    authorizedDate TEXT,
    authorizedDatetime TEXT,
    location TEXT DEFAULT "{}", -- json string object {"address": "300 Post St", "city": "San Francisco", "region": "CA", "postal_code": "94108", "country": "US", "lat": 40.740352, "lon": -74.001761, "store_number": "1235"}
    transactionName TEXT,
    merchantName TEXT,
    paymentMeta TEXT DEFAULT "{}", -- json string object {"by_order_of": null, "payee": null, "payer": null, "payment_method": null, "payment_processor": null, "ppd_id": null, "reason": null, "reference_number": null}
    paymentChannel
    pending INTEGER DEFAULT NULL, -- boolean
    pendingTransactionId TEXT DEFAULT NULL,
    accountOwner TEXT DEFAULT NULL,
    transactionCode TEXT DEFAULT NULL,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX plaidTransaction_transactionId ON plaidTransaction(transactionId);
CREATE INDEX plaidTransaction_accountId ON plaidTransaction(accountId);

DROP TRIGGER IF EXISTS plaidTransaction_afterUpdate;
CREATE TRIGGER plaidTransaction_afterUpdate
    AFTER UPDATE
    ON plaidTransaction
    WHEN OLD.updatedAt IS NULL OR NEW.updatedAt < OLD.updatedAt -- If the recursive_triggers config variable is true, then this "NEW < OLD" check prevents an infinite loop
        BEGIN
            UPDATE plaidTransaction
            SET updatedAt = CURRENT_TIMESTAMP
            WHERE id = OLD.id;
        END;

COMMIT;
