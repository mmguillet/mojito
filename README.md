# Mojito
Personal Finance

## Getting Started

* [Plaid Python Quickstart](https://github.com/plaid/quickstart/tree/master/python)
* [Plaid Data Dictionary](https://plaid.com/exchange/docs/data-dictionary/)
* [Plaid API Docs](https://plaid.com/docs/api/#endpoint-and-schema-overview)

### Mostly Automated Steps

#### Nth time setup

```
source src/venv/bin/activate
python3 src/server.py
```

#### First time setup
From the root project directory, run the following:
```
sh getting-started.sh
source src/venv/bin/activate
pip install --upgrade pip
pip install -r src/requirements.txt
python3 src/server.py
```
Navigate to `https://localhost:8000` in your browser


Get a Plaid permanent access_token:
* Go to https://localhost:8000
  * Open the console
  * Click the Plaid continue button and link a bank account
  * From the console, copy the public_token
* Open your `src/.env` file and paste the public_token near the bottom for PLAID_PUBLIC_TOKEN
* Go to https://localhost:8000/api/set_access_token
  * The access_token is permanent and will not expire, it can also be saved but might not be referenced in the code at the moment
* Go to https://localhost:8000/api/transactions
  * You should see your account balances and/or recent transactions


### Manual Steps
In the `/src` directory
* Make a copy of `.env.example` and name it `.env`
* Open the file and follow the instructions about adding your variables
* (optional) Setup python virtual environment
  * `python3 -m venv venv`
  * `source venv/bin/activate`
  * Use command `deactivate` at any time to exit the virtual environment
* Install requirements `pip install -r requirements.txt`
* Start the flask server `sh start.sh`
* Navigate to `https://localhost:8000/api/status`


### Plaid
Get API keys
* Create account https://dashboard.plaid.com/signup
  * Verify email
  * Sandbox API environment
    * You can now instantly use your username/password for the sandbox API environment
    * Limited to 1 fake data account
* Get live data API keys [1+ day approval time, free]
  * Login and navigate to https://dashboard.plaid.com/overview
  * Request access for the "100 Live Bank Accounts" free option
    * Give phone number and description `A personal finance application with integrations for most likely <15 financial institutions`
  * Wait for an email with subject similar to "Request for Development access" that confirms access
  * Login and navigate to https://dashboard.plaid.com/overview/development


## Features

### Minimum
* Check bank account balances

### Future
* Check credit card balances
* Track credit card due dates
* Track gift cards and balances
* Track coupons and hide on expiration
* Configurable reminders (email)
* Send reminder when coupons are expiring
* Track town tax invoices (car, sewer)
* Track mortgage payments and escrow balance for property tax payments
* Click to add expenses to a shared list (just integrate with a google sheet?)
  * Add filtering by time periods
  * If 2 people have access, give option to balance/diff what 1 person owes over the other
* Model future financial outcomes
  * 401k contribution changes when employer match has a max per paycheck
  * Taxable income / tax bracket changes
    * Sale of stock, company buyout stock options, restricted stock unit/cash pool
    * Bonuses
    * Estimated raises
* [Example personal finance app](https://github.com/plaid/pattern)
